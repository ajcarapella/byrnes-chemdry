<?php get_template_part('includes/header'); ?>

<div class="container cms-page cms-stick">
  <div class="row">
    <div class="col-xs-12 col-sm-8 z-ind-high no-pad">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/content', 'page'); ?>
      </div><!-- /#content -->
    </div>
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/estimate-form'); ?>

<?php get_template_part('includes/footer'); ?>
