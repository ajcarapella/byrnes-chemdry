<footer class="site-footer">
  <div class="row">
    <div class="col-lg-12 site-sub-footer">
      <p>&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>">Byrnes Chem-Dry</a><br>Powered by Total Advertising</p>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
