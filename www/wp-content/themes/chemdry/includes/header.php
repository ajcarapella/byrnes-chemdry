<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title(''); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/favicon-16x16.png">
	<link rel="manifest" href="../images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96749499-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->
<nav class="navbar navbar-default navbar-static-top">
<div class="container-fluid no-pad top-nav-container sticky-hide-mob">
	<div class="container no-pad">
		<div class="row no-pad">
			 <!--section 1-->
			 <div class="col-xs-6 col-sm-2">
			 	<a href="<?php echo home_url('/'); ?>">
				 	<img class="main-logo" src="<?php echo home_url('/'); ?>/wp-content/themes/chemdry/images/logo.jpg" alt="Byrnes Chemdry"/>
			 	</a>
			 </div>
			 
			 
			 <div class="hidden-xs col-sm-4">
			 	<div id="static-ticker">
				 	  <ul>
					    <li>- Spring cleaning special, act now before it’s gone. Add a room for just $49. Minimum may apply.</li>
					    <li>- Refer a friend and you both save.</li>
					  </ul>
			 	</div>
			 </div>
			 
			 
			 	  <!--section 2-->
			 <div class="col-xs-4 col-sm-3">
				<span id="es-text" class="hidden-xs">24/7 Emergency <br>Service Available</span>
			 </div>
			 
			 
			 <!--section 3-->
			 <div class="col-xs-6 col-sm-3 mobile-remove-pad">
				 <span id="fq-text">Call now for a free quote <br>or use the form below.</span>
				 <span id="fq-phone"><a href="tel:3154683184">315.468.3184</a></span>
			 </div>
			 
		
			 
			 
		<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
			<div id="v-container" class="hidden-sm hidden-md hidden-lg">
					<div id="v-ticker">
						  <ul>
						    <li>- Spring cleaning special, act now before it’s gone. Add a room for just $49. Minimum may apply.</li>
						    <li>- Refer a friend and you both save.</li>
						  </ul>
					</div>
				
					<div id="v-arrows">
						 <div id="v-prev"> <img src="<?php echo home_url('/'); ?>/wp-content/themes/chemdry/images/tick-up.jpg" alt="Previous Offer"/> </div>
						 <div id="v-next"> <img src="<?php echo home_url('/'); ?>/wp-content/themes/chemdry/images/tick-down.jpg" alt="Next Offer"/> </div>
					</div>
				</div>

		</div>
			 
	
		</div>
	</div>
</div>
  <div class="container-fluid no-pad bottom-nav-container">
    <div class="navbar-header">
	    <span id="es-text-mob" class="hidden-sm hidden-md hidden-lg">24/7 Emergency Service Available</span>
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
        <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar top-bar"></span>
	        <span class="icon-bar middle-bar"></span>
	        <span class="icon-bar bottom-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar">
             <?php //get_template_part('includes/navbar-search'); ?>
       <?php
            wp_nav_menu( array(
                'theme_location'    => 'navbar-left',
                'depth'             => 2,
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>
        
     

     </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>

<div class="overlay-nav"></div>

