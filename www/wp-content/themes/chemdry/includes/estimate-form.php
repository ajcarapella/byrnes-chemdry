<?php
/*
Get Estimate Form
=====================================================
*/
?>

 <div class="ef-container">
	   <div class="container estimate-form cms-ef">
					<div id="title-bar">
						FREE Estimate and <br>10% off Coupon
						<em>When you fill out and schedule an appointment.</em>
					</div>
					<?php echo do_shortcode( '[contact-form-7 id="12" title="Schedule Your Free Consultation"]' );?>
					<div id="thank-you-message">
						<h2>Thank you for<br> Your Submission!</h2>
						<p>Someone will get back to you shortly with information to set up your appointment.</p>
					</div>
					<div class="close-estimate"><img class="expandbut" src="<?php echo home_url('/'); ?>/wp-content/themes/chemdry/images/arrowUp.png"/><img class="closebut" src="<?php echo home_url('/'); ?>/wp-content/themes/chemdry/images/closebut.png"/></div>
		</div>
   </div>
	
<div class="estimate-overlay"></div>