<?php /*
Template Name: Home Page
*/ 
get_template_part('includes/header'); ?>


<!-- Home Slider -->

<div id="homeCarousel" class="carousel slide cms-home-page cms-stick">
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/Slider1.jpg" alt="Slider 1"/>
            <div class="carousel-caption blue-caption">
            	<h2>Get a jump start on spring cleaning</h2>
            	<h3>Add a room for just <sup>$</sup>49. Minimum may apply.
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/Slider2.jpg" alt="Slider 2"/>
            <div class="carousel-caption white-caption">
            	<h2>Your healthy home starts here</h2>
            	<h3>Take advantage of our many services including <br>carpet, rug, upholstery and drapery cleaning.
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/Slider3.jpg" alt="Slider 3"/>
            <div class="carousel-caption white-caption">
            	<h2>There when you need us most</h2>
            	<h3>We have a 24/7 water damage restoration <br>emergency response team.
            </div>
        </div>
        <div class="item">
            <img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/Slider4.jpg" alt="Slider 4"/>
            <div class="carousel-caption blue-caption">
            	<h2>We clean for your health</h2>
            	<h3>Our environmentally friendly products ensure <br>you are getting the safest clean for your home.
            </div>
        </div>
        
	    <ol class="carousel-indicators">
	        <li data-target="#homeCarousel" data-slide-to="0" class="active" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="1" class="" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="2" class="" contenteditable="false"></li>
	        <li data-target="#homeCarousel" data-slide-to="3" class="" contenteditable="false"></li>
	    </ol>
	    
    </div>    
    
    
        
    
    

</div>

<!-- End Carousel -->


<div class="container no-pad-dt">
  <div class="row">
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon1.png" alt="Carpet & Rug Cleaning"/></a>
	    <span>Carpet &amp; Rug Cleaning</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#carpet"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon2.png" alt="Upholstery & Drapery Cleaning"/></a>
	    <span>Upholstery &amp; Drapery Cleaning</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#upholstery"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon3.png" alt="Water Damage Restoration"/></a>
	    <span>Water Damage Restoration</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#water"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon4.png" alt="Pet Stains & Odor Removal"/></a>
	    <span>Pet Stains &amp; Odor Removal</span>
    </div>
  </div><!-- /.row -->
  
  <div class="row hidden-xs">
  	<div class="col-xs-12 col-sm-4 home-point no-pad">
  		<h2>We’ll be there fast- We’re local and just minutes away.</h2>
  		<p class="hidden-xs">If you are looking for quality stain removal in Onondaga County, we can guarantee excellent results. We use the latest products and technology to remove the toughest spills, stains, and odors in your home or business.</p>
  	</div>
  	<div class="col-xs-12 col-sm-4 home-point no-pad">
  		<h2>The expertise you demand with over 24 years experience.</h2>
  		<p class="hidden-xs">Byrnes Chem-Dry is an independently owned and operated company that has served Onondaga County for more than 24 years. Our mission is to provide you with second-to-none carpet cleaning, water extraction, and water damage services.</p>
  	</div>
  	<div class="col-xs-12 col-sm-4 home-point no-pad">
  		<h2>Over 10,000 CNY homeowners and businesses trust us.</h2>
  		<p class="hidden-xs">Whether your home or office has been flooded or simply requires a diligent professional cleaning, we offer fast and effective service options to meet your needs, and our emergency services are available for you 24/7.</p>
  	</div>
  </div><!-- /.row -->  
  
  
  <div class="row hidden-sm hidden-md hidden-lg pt-25-mob">
  	<ul>
  		<li class="list_item">
		    <h2 class="info-section-title">We’ll be there fast- We’re local and just minutes away.</h2>
		    <div class="info-section-info"><p>If you are looking for quality stain removal in Onondaga County, we can guarantee excellent results. We use the latest products and technology to remove the toughest spills, stains, and odors in your home or business.</p>
		    </div>
		</li>
		
		<li class="list_item">
		    <h2 class="info-section-title">The expertise you demand with over 20 years experience.</h2>
		    <div class="info-section-info"><p>Byrnes Chem-Dry is an independently owned and operated company that has served Onondaga County for more than 21 years. Our mission is to provide you with second-to-none carpet cleaning, water extraction, and water damage services.</p>
		    </div>
		</li>
		
		<li class="list_item">
		    <h2 class="info-section-title">Over 10,000 CNY homeowners and businesses trust us.</h2>
		    <div class="info-section-info"><p>Whether your home or office has been flooded or simply requires a diligent professional cleaning, we offer fast and effective service options to meet your needs, and our emergency services are available for you 24/7.</p>
		    </div>
		</li>
  	</ul>
  
  </div>
  
  
   <div class="row">
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#pet"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon5.png" alt="Carpet & Rug Cleaning"/></a>
	    <span>24/7 Service</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#service"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon6.png" alt="Upholstery & Drapery Cleaning"/></a>
	    <span>Environmentally Friendly</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#environment"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon7.png" alt="Water Damage Restoration"/></a>
	    <span>Spot Treatments</span>
    </div>
    
    <div class="col-xs-3 home-service">
	    <a href="<?php echo home_url('/'); ?>services#spot"><img src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/icon8.png" alt="Pet Stains & Odor Removal"/></a>
	    <span>Satisfaction Guaranteed</span>
    </div>
  </div><!-- /.row -->
  
  <div style="clear:both;">
  </div>
  
  <div class="row before-after">
  		 <div class="col-xs-12 col-sm-12 ba-image-container no-pad">
	  		
	  			    <img class="hidden-xs w-100" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/B-A-Home.jpg" alt="Before After Carpet"/>
	  			    <img class="hidden-sm hidden-md hidden-lg w-100" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/B-A-Home-Mob.jpg" alt="Before After Carpet"/>
	  		
  		</div>
  		
  		
  </div>
  
  
  
  <div class="row no-pad">
  	<div class="col-xs-12 no-pad">
  		<img class="w-100 d-table pb-20 hidden-xs" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/certs.jpg" alt="Certifications"/>
  		<img class="w-100 d-table pb-20 hidden-sm hidden-md hidden-lg" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/m_certs.jpg" alt="Certifications"/><br>
  	</div>
  </div>
  
</div><!-- /.container -->


<?php get_template_part('includes/estimate-form'); ?>

<?php get_template_part('includes/footer'); ?>
