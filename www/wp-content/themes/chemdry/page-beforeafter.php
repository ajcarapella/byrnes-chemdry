<?php /*
Template Name: Before and After Page
*/ 
get_template_part('includes/header'); ?>

	<div class="container cms-page cms-stick">
	  <div class="row">
	    <div class="col-xs-12 col-sm-7 col-md-8 z-ind-high no-pad">
	      <div id="content" role="main">
	        <?php get_template_part('includes/loops/content', 'page'); ?>
	        
			
			
			<!-- Testimonial 1-->     
			  <div class="row before-after ba-cms-top">
			  		 <div class="col-xs-12 ba-image-container no-pad">
				  			    <img class="w-100" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/B-A-1.jpg" alt="Before Carpet"/>
				  		
			  		</div>
			  </div>	
	
			
			<!-- Testimonial 2-->     
			  <div class="row before-after ba-cms-top">
				  	<img class="w-100" src="<?php echo home_url('/'); ?>wp-content/themes/chemdry/images/B-A-2.jpg" alt="Before Carpet"/>
			  </div>	
			  			  
			  
			
				 			  
	
	        
	        
	      </div><!-- /#content -->
	    </div>
	  </div><!-- /.row -->
	</div><!-- /.container -->


<?php get_template_part('includes/estimate-form'); ?>

<?php get_template_part('includes/footer'); ?>