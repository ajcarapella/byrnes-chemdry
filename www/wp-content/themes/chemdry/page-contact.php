<?php /*
Template Name: Contact Page
*/ 
get_template_part('includes/header'); ?>

<div class="container cms-page cms-stick">
  <div class="row">
    <div class="col-xs-12 col-sm-7 col-md-8 z-ind-high no-pad">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/content', 'page'); ?>
        
       	 <div class="row">
			  <div class="col-xs-12 contact-box-container">
				  <div class="contact-box">
				  	<h2>Byrnes Chem-Dry</h2>
				  	<div class="row">
					  	<div class="col-xs-6" style="padding-left:0;">
						  	<h3>114 Spruce Tree Ln</h3>
						  	<h3>Syracuse, NY 13219</h3>
					  	</div>

				  	<div class="col-xs-6">
					  	<h3>315.468.3184</h3>
					  	<h3>info@byrneschemdry.com</h3>
				  	</div>
				  	</div>
				  </div>
			  </div>
			  
			  <div class="col-xs-12 col-sm-12 no-pad">
				  <iframe id="google-map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2915.8463132172915!2d-76.226952!3d43.044665!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d9f05eaaaaaaab%3A0xcd3f1b92cf771dd1!2sByrnes+Chem-Dry!5e0!3m2!1sen!2sus!4v1426521736937" width="600" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
				  
				  
				  
				  
				  
				 
				  
			  </div>
			  
			  
		  </div><!-- /.row -->


		  <?php echo do_shortcode('[contact-form-7 id="41" title="Contact Form"]');?>
        
                
      </div><!-- /#content -->
    </div>
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('includes/estimate-form'); ?>

<?php get_template_part('includes/footer'); ?>