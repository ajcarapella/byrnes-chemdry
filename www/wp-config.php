<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'byrnescd_chemdry');

/** MySQL database username */
define('DB_USER', 'byrnescd_root');

/** MySQL database password */
define('DB_PASSWORD', '4Total1!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ib):UjF1ZGU4A=l0IdYi 51xM<n2y(hnrVZ*}T}-^QkG@lN4C:Sy0:v-yE(ED|sO');
define('SECURE_AUTH_KEY',  'DDip<+2x^PA#j|[WBZ`%3&dk3OPs5J4Ovb5[1k%3>z[}1Z[7yR%f@?8|u(P@8T,~');
define('LOGGED_IN_KEY',    ')WFnQ7[H;-ym{^K}gHzgz}W)9/D>yw=## |_t7N+ I:b..2DUi2yj7|~bMp75=5e');
define('NONCE_KEY',        'SM0dGUwfEPLmdR5[2Neg=1sjoFGmEjGlW|c-vHJl,l mXO7^Snx/aH-F`LZh;fv}');
define('AUTH_SALT',        'js|-{Y=ljl%x~vIvcv]l#=c!-LZQFrI-3Hg?qU6xe6(HS^p|D3o4|Bk44}ey0Wa{');
define('SECURE_AUTH_SALT', '`WI8H,e&Tz_EYE0#0@_jNS&*h5iF--@/~v>Lb$;vi/luh]d]O`Fzls=J+aaW3,k+');
define('LOGGED_IN_SALT',   ',NShr#0v2}M}bkHy/!aFe(m`6*2dWDOGqAC;JCL!NLi[Tc(8tsw:xk!{#|qJ/.rv');
define('NONCE_SALT',       'bG_JGQAK/?0[k_D~?LVXC;OP1EUPyTqo35/WU-J@HU+6d$lwsS@UOQyV}l<B+^sm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
